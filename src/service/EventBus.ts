export enum LangEvent {
  LANG_CHANGE,
  READY,
}

type EventBusPool = {
  [key: number]: Function[],
};

export class EventBus {
  private pool: EventBusPool;

  constructor(events: LangEvent[]) {
    this.pool = events.reduce((out, event) => ({
      ...out,
      [event]: [],
    }), {});
  }

  on(event: LangEvent, handler: Function) {
    if (this.pool[event].includes(handler)) {
      return;
    }

    this.pool[event].push(handler);
  }

  off(event: LangEvent, handler: Function) {
    this.pool[event].splice(this.pool[event].indexOf(handler, 1));
  }

  emit(event: LangEvent, ...payload: any[]) {
    this.pool[event].forEach((h) => h(...payload));
  }
}
