import { Language, LanguageIso6391, LanguageMap } from '../enums';

export function preventEmptyStringRewriteCustomizer(objValue: object | string, srcValue: string) {
  if (typeof objValue === 'string' && !srcValue.trim?.()) {
    return srcValue;
  }
  return undefined;
}

export function getNavigatorLanguage(availableLangs: LanguageIso6391[]): Language | null {
  const navigatorLanguages = navigator.languages ?? [];
  const navigatorLanguage = navigator.language;
  const navigatorLocales = [...navigatorLanguages, navigatorLanguage].filter(Boolean);

  // eslint-disable-next-line no-restricted-syntax
  for (const navigatorLocale of navigatorLocales) {
    const languageCode = navigatorLocale.split('-')[0].toUpperCase() as LanguageIso6391;
    const language = LanguageMap.getItem(languageCode);

    if (language && availableLangs.includes(language.iso6391)) {
      return language;
    }
  }

  return null;
}

export function objectFlat(
  object: object,
  namespace: string = '',
  delimiter: string = '.',
): Record<string, string> {
  let result = {};

  Object.entries(object).forEach(([key, value]) => {
    if (value instanceof Object) {
      result = {
        ...result,
        ...objectFlat(value, `${namespace ? `${namespace}${delimiter}` : ''}${key}`),
      };
    } else {
      // @ts-ignore
      result[`${namespace ? `${namespace}${delimiter}` : ''}${key}`] = value;
    }
  });

  return result;
}
