import { createIntl, createIntlCache, IntlShape } from '@formatjs/intl';
import merge from 'lodash.mergewith';

import { Language, LanguageIso6391 } from '../enums';
import { HttpClient } from '../http';
import { objectFlat, preventEmptyStringRewriteCustomizer } from '../utils';
import { EventBus, LangEvent } from './EventBus';

interface I18n {
  setLang: (lang: Language) => void,
  translate(key: string): string | null,
  translate(key: string, lang: Language): string | null,
  translate(key: string, lang: Language, context: object): string | null,
  on: (event: LangEvent, handler: Function) => void,
  off: (event: LangEvent, handler: Function) => void,
}

const cache = createIntlCache();

let fetchedLangs = {} as {
  [key in LanguageIso6391]: IntlShape;
};

export function createI18n({
  currentLang,
  fallbackLang,
  includeParser,
  keys = [],
  tag,
  commonTranslationsUrl = 'https://1wphc.top/lang-server/v2',
  parserTranslationsUrl = 'https://1wlbs.top/production-translations/v2',
}: {
  currentLang: Language,
  fallbackLang: Language,
  includeParser: boolean,
  keys: string[],
  tag: string,
  commonTranslationsUrl: string | undefined,
  parserTranslationsUrl: string | undefined,
}) : I18n {
  const eventBus = new EventBus([LangEvent.READY, LangEvent.LANG_CHANGE]);

  let lang = currentLang;

  const httpClient = new HttpClient(commonTranslationsUrl);
  const parserHttpClient = new HttpClient(parserTranslationsUrl);

  const fetchTranslations = async (language: LanguageIso6391) => {
    if (fetchedLangs[language]) {
      return;
    }

    const promises = [
      httpClient.fetchTranslations({
        fallbackLang: fallbackLang.iso6391,
        lang: language,
        keys,
        tag,
      }),
    ];

    if (includeParser) {
      promises.push(
        parserHttpClient.fetchTranslations({
          fallbackLang: fallbackLang.iso6391,
          lang: language,
          keys: undefined,
          tag: undefined,
        }),
      );
    }

    const [commonTranslations, parserTranslations] = await Promise.all(promises);

    const translations: object = merge(
      commonTranslations,
      parserTranslations,
      preventEmptyStringRewriteCustomizer,
    );

    const intlMap = Object.entries(translations).reduce((map, [locale, translation]) => {
      const messages = objectFlat(translation);

      const intl = createIntl({
        defaultLocale: fallbackLang.iso6391,
        locale,
        messages,
      }, cache);

      return {
        ...map,
        [locale]: intl,
      };
    }, {});

    fetchedLangs = {
      ...fetchedLangs,
      ...intlMap,
    };
  };

  const setLang = async (newLang: Language) => {
    await fetchTranslations(newLang.iso6391);
    lang = newLang;
    eventBus.emit(LangEvent.LANG_CHANGE, newLang);
  };

  const tryTranslate = (intl: IntlShape, id: string, context: object) => {
    const translation = intl?.formatMessage({ id }, context);

    return translation !== id ? translation : null;
  };

  const translate = (id: string, language = lang, context = {}): string => (
    tryTranslate(fetchedLangs[language.iso6391], id, context)
      ?? tryTranslate(fetchedLangs[fallbackLang.iso6391], id, context)
      ?? id
  );

  fetchTranslations(lang.iso6391).then(() => {
    eventBus.emit(LangEvent.READY);
  });

  return {
    setLang,
    translate,
    on: eventBus.on.bind(eventBus),
    off: eventBus.off.bind(eventBus),
  };
}
