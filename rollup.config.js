/** Общие плагины, необходимые для работы сборки */
import commonjs from '@rollup/plugin-commonjs';
import replace from '@rollup/plugin-replace';
import typescript from '@wessberg/rollup-plugin-ts';
/** Плагины, необходимые для cjs production сборки */
import { writeFile } from 'fs/promises';
import { terser } from 'rollup-plugin-terser';

/** Сборщик полагается на main и module поля package.json для определения выходных имён файлов */
import pkg from './package.json';

/** Небольшой "разветвитель" для cjs сборки */
const cjsEntryPoint = `'use strict';
if (process.env.NODE_ENV === 'production') {
  module.exports = require('./index.cjs.production.js');
} else {
  module.exports = require('./index.cjs.development.js');
}
`;

/** Плагин для поддержки cjs прод сборки */
const cjs = () => ({
  name: 'cjs',
  /** Плагин replace не предназначен для использования в качестве output-плагина, обходим это ограничение */
  renderChunk: (...args) => replace({ 'process.env.NODE_ENV': "'production'" }).renderChunk(...args),
  /** По окончании сборки дописываем наш "разветвитель" */
  writeBundle: () => writeFile('dist/index.cjs.js', cjsEntryPoint, 'utf8'),
});

export default {
  input: 'src/index.ts',
  external: ['@formatjs/intl', 'lodash.mergewith', 'axios', 'query-string'],

  output: [
    {
      file: pkg.main.replace('.js', '.development.js'),
      format: 'cjs',
      sourcemap: true,

    },
    {
      file: pkg.main.replace('.js', '.production.js'),
      format: 'cjs',
      sourcemap: true,
      plugins: [
        cjs(),
        terser(),
      ],
    },
    {
      file: pkg.module,
      format: 'esm',
      sourcemap: true,
    },
  ],
  plugins: [
    commonjs(),
    typescript({
      include: 'src/**',
      transpiler: 'babel',
      tsconfig: (config) => ({
        ...config,
        target: 'ES5',
      }),
      hook: {
        outputPath: (path, kind) => {
          if (kind === 'declaration') {
            return 'dist/index.d.ts';
          }
          return path;
        },
      },
    }),
  ],
};
