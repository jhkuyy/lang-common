import axios, { AxiosInstance } from 'axios';
import qs from 'query-string';

import { LanguageIso6391 } from '../enums';

export class HttpClient {
  private client: AxiosInstance;

  constructor(baseUrl: string) {
    this.client = axios.create({
      baseURL: baseUrl,
    });
  }

  fetchTranslations({
    fallbackLang,
    lang,
    keys = [],
    tag,
  }: {
    fallbackLang: LanguageIso6391,
    lang: LanguageIso6391,
    keys: string[] | undefined,
    tag: string | undefined,
  }) {
    return this.client.request({
      method: 'get',
      params: {
        langs: [lang, fallbackLang].filter((it) => it),
        keys,
        tag,
      },
      paramsSerializer: (params) => qs.stringify(params),
    }).then(({ data }) => data);
  }
}
