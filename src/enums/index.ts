export type LanguageIso6391 =
  'Al' | 'al'
  | 'AM' | 'am'
  | 'AZ' | 'az'
  | 'BY' | 'by'
  | 'CZ' | 'cz'
  | 'DE' | 'de'
  | 'EN' | 'en'
  | 'ES' | 'es'
  | 'FR' | 'fr'
  | 'GE' | 'ge'
  | 'ID' | 'id'
  | 'IN' | 'in'
  | 'IR' | 'ir'
  | 'IT' | 'it'
  | 'KG' | 'kg'
  | 'KK' | 'kk'
  | 'MD' | 'md'
  | 'PL' | 'pl'
  | 'PT' | 'pt'
  | 'RO' | 'ro'
  | 'RU' | 'ru'
  | 'TH' | 'th'
  | 'TJ' | 'tj'
  | 'TR' | 'tr'
  | 'UA' | 'ua'
  | 'UZ' | 'uz'
  | 'ZA' | 'za';

export interface Language {
  name: string,
  iso6391: LanguageIso6391,
  iso6392: string,
}

function createLanguageEnumItem(
  iso6391: LanguageIso6391,
  iso6392: string,
  name: string,
): Language {
  return Object.freeze({
    iso6391,
    iso6392,
    name,
    toString() {
      return iso6391;
    },
  });
}

export const LanguageMap = Object.freeze({
  Al: createLanguageEnumItem('al', 'alb', 'Shqiptare'),
  AM: createLanguageEnumItem('am', 'arm', 'հայերեն'),
  AZ: createLanguageEnumItem('az', 'aze', 'Azərbaycan dili'),
  BY: createLanguageEnumItem('by', 'blr', 'Беларускі'),
  CZ: createLanguageEnumItem('cz', 'cze', 'čeština'),
  DE: createLanguageEnumItem('de', 'deu', 'Deutsch'),
  EN: createLanguageEnumItem('en', 'eng', 'English'),
  ES: createLanguageEnumItem('es', 'esp', 'Espanol'),
  FR: createLanguageEnumItem('fr', 'fre', 'Le français'),
  GE: createLanguageEnumItem('ge', 'geo', 'ქართველი'),
  ID: createLanguageEnumItem('id', 'idn', 'Indonesian'),
  IN: createLanguageEnumItem('in', 'ind', 'हिन्दी'),
  IR: createLanguageEnumItem('ir', 'irn', ''), //   ????????
  IT: createLanguageEnumItem('it', 'ita', 'Italiano'),
  KG: createLanguageEnumItem('kg', 'kgz', 'Кыргызча'),
  KK: createLanguageEnumItem('kk', 'kaz', 'Қазақ'),
  MD: createLanguageEnumItem('md', 'mda', 'Moldovan'),
  PL: createLanguageEnumItem('pl', 'pol', 'Polski'),
  PT: createLanguageEnumItem('pt', 'prt', 'Português'),
  RO: createLanguageEnumItem('ro', 'rou', 'Română'),
  RU: createLanguageEnumItem('ru', 'rus', 'Русский'),
  TH: createLanguageEnumItem('th', 'tha', 'ไทย'),
  TJ: createLanguageEnumItem('tj', 'tjk', 'Тоҷикӣ'),
  TR: createLanguageEnumItem('tr', 'tur', 'Türk'),
  UA: createLanguageEnumItem('ua', 'ukr', 'Український'),
  UZ: createLanguageEnumItem('uz', 'uzb', 'O\'zbek'),
  ZA: createLanguageEnumItem('za', 'zaf', 'Kiswahili'),
  getItem(key: LanguageIso6391): Language | null {
    // @ts-ignore
    return this[key.toUpperCase()] ?? null;
  },
});
